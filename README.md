# Model train ambient Light controller

Uses an ESP8266 and a Neopixel Strip to control the ambient lighting for a model railway display box.

Data pin of Neopixel Strip is connected to pin D1 on ESP. +5V from Power supply to Vin of ESP8266 and Neopixel strip, GND of power supply to GND of ESP8266 and Neopixel.

## Feautures
* Sunset/Sunrise simulation
* OTA Updates
* Controlled via http (Website or GET request from other ESP/Raspberry/...)

## TODO
* Make more configurable
* Thunderstorm simulation (mabye with sound)
 
